# SQL
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String, DECIMAL, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy_utils import EncryptedType
from sqlalchemy_utils.types.encrypted.encrypted_type import AesEngine

from .db_connection import *


class GridSettingsItemDB(db_connection.base):
    __tablename__ = 'grid_settings_item'
    secret_key = ''
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer)
    grid_item_id = Column(Unicode(200))
    grid_x_pos = Column(Integer)
    grid_y_pos = Column(Integer)
    grid_width = Column(Integer)
    grid_height = Column(Integer)


    def __init__(self, user_id, grid_item_id, grid_x_pos, grid_y_pos, grid_width, grid_height):
        self.user_id = user_id
        self.grid_item_id = grid_item_id
        self.grid_x_pos = grid_x_pos
        self.grid_y_pos = grid_y_pos
        self.grid_width = grid_width
        self.grid_height = grid_height

    def __repr__(self):
        return f'<GridSettingsItemDB {self.id}, {self.user_id}, {self.grid_item_id}, {self.grid_x_pos}, {self.grid_y_pos}, {self.grid_width}, {self.grid_height}>'

db_connection.base.metadata.create_all() 
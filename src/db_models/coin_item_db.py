# SQL
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String, DECIMAL, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy_utils import EncryptedType
from sqlalchemy_utils.types.encrypted.encrypted_type import AesEngine

from .db_connection import *


class CoinItemDB(db_connection.base):
    __tablename__ = 'coin_item'
    secret_key = ''
    user_id = Column(Integer)
    id = Column(Integer, primary_key=True)
    ticker = Column(Unicode(350))
    name = Column(Unicode(500))
    
    def __init__(self, user_id, ticker, name):
        self.user_id = user_id
        self.ticker = ticker
        self.name = name

    def __repr__(self):
        return f'<CoinItemDB {self.id}, {self.user_id}, {self.ticker}, {self.name}>'

db_connection.base.metadata.create_all() 
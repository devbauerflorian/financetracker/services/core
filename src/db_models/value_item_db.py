# SQL
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String, DECIMAL, ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy_utils import EncryptedType
from sqlalchemy_utils.types.encrypted.encrypted_type import AesEngine

from .db_connection import *


class ValueItemDB(db_connection.base):
    __tablename__ = 'value_item'
    secret_key = ''
    id = Column(Integer, primary_key=True)
    value = Column(EncryptedType(DECIMAL,
                                secret_key,
                                AesEngine,
                                'pkcs5'))
    currency = Column(EncryptedType(Unicode,
                                secret_key,
                                AesEngine,
                                'pkcs5'))

    @staticmethod
    def set_encryption_key(secret_key):
        secret_key=secret_key
   
    def __init__(self, value, currency):
        self.value = value
        self.currency = currency

    def __repr__(self):
        return f'<ValueItemDB {self.value}, {self.currency}>'

db_connection.base.metadata.create_all() 
# SQL
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from .db_connection import *

class ArtistDB(db_connection.base):
    __tablename__ = 'artists'
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(40))
    username = Column(String(20))
    albumnr = Column(Integer)

    def __init__(self, name, username, albumnr):
        self.name = name
        self.username = username
        self.albumnr = albumnr

db_connection.base.metadata.create_all() 
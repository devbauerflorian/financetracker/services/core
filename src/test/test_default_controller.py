# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from openapi_server.models.balance_item import BalanceItem  # noqa: E501
from openapi_server.models.category_item import CategoryItem  # noqa: E501
from openapi_server.models.coin_item import CoinItem  # noqa: E501
from openapi_server.models.coin_position import CoinPosition  # noqa: E501
from openapi_server.models.grid_settings_item import GridSettingsItem  # noqa: E501
from openapi_server.models.inline_response200 import InlineResponse200  # noqa: E501
from openapi_server.models.inline_response2001 import InlineResponse2001  # noqa: E501
from openapi_server.models.inline_response400 import InlineResponse400  # noqa: E501
from openapi_server.models.share_item import ShareItem  # noqa: E501
from openapi_server.models.share_position import SharePosition  # noqa: E501
from openapi_server.models.transaction_item import TransactionItem  # noqa: E501
from openapi_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_balance_current_get(self):
        """Test case for balance_current_get

        
        """
        pass

    def test_balance_list_get(self):
        """Test case for balance_list_get

        
        """
        pass

    def test_categories_get(self):
        """Test case for categories_get

        
        """
        pass

    def test_category_category_id_delete(self):
        """Test case for category_category_id_delete

        
        """
        pass

    def test_category_category_id_put(self):
        """Test case for category_category_id_put

        
        """
        pass

    def test_category_post(self):
        """Test case for category_post

        
        """
        pass

    def test_coin_position_get(self):
        """Test case for coin_position_get

        
        """
        pass

    def test_coin_position_post(self):
        """Test case for coin_position_post

        
        """
        pass

    def test_coin_type_coin_type_id_delete(self):
        """Test case for coin_type_coin_type_id_delete

        
        """
        pass

    def test_coin_type_post(self):
        """Test case for coin_type_post

        
        """
        pass

    def test_coin_types_get(self):
        """Test case for coin_types_get

        
        """
        pass

    def test_grid_settings_get(self):
        """Test case for grid_settings_get

        
        """
        pass

    def test_grid_settings_put(self):
        """Test case for grid_settings_put

        
        """
        pass

    def test_share_position_get(self):
        """Test case for share_position_get

        
        """
        pass

    def test_share_position_post(self):
        """Test case for share_position_post

        
        """
        pass

    def test_share_type_post(self):
        """Test case for share_type_post

        
        """
        pass

    def test_share_type_share_type_id_delete(self):
        """Test case for share_type_share_type_id_delete

        
        """
        pass

    def test_share_types_get(self):
        """Test case for share_types_get

        
        """
        pass

    def test_transaction_post(self):
        """Test case for transaction_post

        
        """
        pass

    def test_transaction_transaction_id_delete(self):
        """Test case for transaction_transaction_id_delete

        
        """
        pass

    def test_transaction_transaction_id_get(self):
        """Test case for transaction_transaction_id_get

        
        """
        pass

    def test_transaction_transaction_id_put(self):
        """Test case for transaction_transaction_id_put

        
        """
        pass

    def test_transactions_count_get(self):
        """Test case for transactions_count_get

        
        """
        pass

    def test_transactions_history_get(self):
        """Test case for transactions_history_get

        
        """
        pass


if __name__ == '__main__':
    import unittest
    unittest.main()

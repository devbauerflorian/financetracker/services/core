"""
Actual backend implementation
"""
from typing import List

from openapi_server.models.inline_response200 import InlineResponse200  # noqa: E501
from openapi_server.models.inline_response400 import InlineResponse400  # noqa: E501
from openapi_server import util

# SQL
from sqlalchemy import Column, Integer, Unicode, UnicodeText, String
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

# Connection
from ..db_models.db_connection import *

import os
import logging

AUTH_SERVER = os.environ['FINANCETRACKER_OAUTH_SERVER_ROOT_URI']

class SecurityController_interface:


    def info_from_oAuthService(token):
        """
        Validate and decode token.
        Returned value will be passed in 'token_info' parameter of your operation function, if there is one.
        'sub' or 'uid' will be set in 'user' parameter of your operation function, if there is one.
        'scope' or 'scopes' will be passed to scope validation function.

        :param token Token provided by Authorization header
        :type token: str
        :return: Decoded token information or None if token is invalid
        :rtype: dict | None
        """
        #print(f"Called info_from_oAuthService with {token}")

        import requests 
        
        # api-endpoint 
        URL = AUTH_SERVER + "api/token_information"
        # location given here 
        location = "germany"
        # defining a params dict for the parameters to be sent to the API 
        PARAMS = {'access_token':token} 
        # sending get request and saving the response as response object 
        response = requests.get(url = URL, params = PARAMS) 
        token_info = response.json()
        # Request information about token from authentication server

        if (token_info.get('error','invalid request') == "success"):
            logging.info(f"API Access from Token: {token}")
        else:
            print(f"Error: Could not get token information for {token}.")
            return None
        
        return {'scopes': token_info.get('token_scope').split(' '), 'uid': token_info.get('user_id'), 'encryption_key': token_info.get('encryption_key')}


    def validate_scope_oAuthService(required_scopes, token_scopes):
        """
        Validate required scopes are included in token scope

        :param required_scopes Required scope to access called API
        :type required_scopes: List[str]
        :param token_scopes Scope present in token
        :type token_scopes: List[str]
        :return: True if access to called API is allowed
        :rtype: bool
        """
        #print(f"Called validate_scope_oAuthService with required: {required_scopes}, token scope: {token_scopes}")
        return set(required_scopes).issubset(set(token_scopes))
